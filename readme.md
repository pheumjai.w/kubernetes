### install zsh & oh my zsh
1. sudo apt install zsh -y
2. chsh -s $(which zsh)
3. sudo apt install curl wget git -y
4. sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
5. git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
6. sed -i "s/plugins=(git)/plugins=(git zsh-autosuggestions zsh-syntax-highlighting)/g" ~/.zshrc

### install crio(recommend run as root)
1. export OS=xUbuntu_20.04
2. export VERSION=1.18
3. echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list
4. curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | apt-key add -
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | apt-key add -
6. apt update
7. apt install cri-o cri-o-runc -y
8. apt-mark hold cri-o cri-o-runc
9. systemctl enable crio
10. systemctl start crio

### install kubernetes(recommend run as root)
1. swapoff -a
2. cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF
3. cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
4. apt update
5. apt install apt-transport-https ca-certificates curl -y
6. curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
7. echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
8. apt update
9. apt install kubelet kubeadm kubectl -y
10. apt-mark hold kubelet kubeadm kubectl
11. kubeadm init --control-plane-endpoint=cluster-endpoint --pod-network-cidr=10.244.0.0/16

### add authentication to docker
echo -n $TOKEN | docker login https://docker.pkg.github.com -u USERNAME --password-stdin

### add authentication to kubernetes
echo -n $TOKEN | base64
echo '{"auths":{"docker.pkg.github.com":{"auth":"<AUTH>"}}}' | kubectl create secret generic github-com --type=kubernetes.io/dockerconfigjson --from-file=.dockerconfigjson=/dev/stdin

### expose ingress to external
1. curl -L https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip -o ngrok.zip
2. unzip ngrok.zip
3. ./ngrok http -bind-tls=true -region=ap -host-header=demo.local 192.168.1.240
